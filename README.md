Author: [Robinson Dias](https://github.com/robinson-1985)

# Desafio de Projeto sobre Git/GitHub da DIO
Repositório criado para o Desafio de Projeto.

- Nesse projetinho reforçamos o nosso conhecimento em Git com um desafio de projeto totalmente prático, onde executamos todos os passos para a criação, atualização e sincronização de um repositório no GitHub. Para isso, temos em mente todas as dicas e direcionamentos apresentados pelo expert nas aulas. 

- Bootcamp **everis New Talents #2 .NET**

Expert DIO: [Venilton Falvo Jr](https://github.com/falvojr)

## Links Úteis
[Sintaxe Basica Markdown](https://www.markdownguide.org/basic-syntax/)
